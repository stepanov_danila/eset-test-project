package com.stepanov.esetfilefinder.utils;

import androidx.annotation.NonNull;

import java.util.AbstractList;
import java.util.LinkedList;
import java.util.List;

/*
This list was created for better performance during scanning.
The lowest file will be always in 0 position.
Algorithm compares current file with the minimal file in this list
 */
public class SortedFileList extends AbstractList<OptimizedFile> {

    private final List<OptimizedFile> internalList;
    private final int capacity;

    public SortedFileList(int capacity) {
        this.internalList = new LinkedList<>();
        // Linked list was chosen because of time for inserting/removing elements
        this.capacity = capacity;
    }

    @Override
    public boolean add(OptimizedFile file) {
        if (internalList.isEmpty()) {
            // For first file we simply add one
            internalList.add(file);
            return true;
        }

        // Going from tail to head and insert new file in ordered position
        for (int i = internalList.size() - 1; i > -1; i--) {
            if (file.size > internalList.get(i).size) {
                internalList.add(i + 1, file);
                break;
            }
        }

        // If size of list bigger than capacity after inserting, then remove first (minimal) element
        if (internalList.size() > capacity) {
            internalList.remove(0);
        }
        return true;
    }

    @Override
    public OptimizedFile get(int i) {
        return internalList.get(i);
    }

    @Override
    public int size() {
        return internalList.size();
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (OptimizedFile f : internalList) {
            builder.append("(name=")
                    .append(f.uri)
                    .append("|size=")
                    .append(f.size)
                    .append("),\n");
        }
        builder.append("]");
        return "SortedFileList{" +
                "internalList=" + builder.toString() +
                ", capacity=" + capacity +
                '}';
    }

    public OptimizedFile getMinimalFile() {
        return internalList.get(0);
    }
}
