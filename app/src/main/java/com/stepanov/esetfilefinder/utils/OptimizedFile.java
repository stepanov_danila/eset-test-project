package com.stepanov.esetfilefinder.utils;

import android.net.Uri;

public class OptimizedFile {

    public final Uri uri;
    public final long size;

    public OptimizedFile(Uri uri, long size) {
        this.uri = uri;
        this.size = size;
    }
}
