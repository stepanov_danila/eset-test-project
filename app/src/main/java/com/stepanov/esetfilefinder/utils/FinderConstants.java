package com.stepanov.esetfilefinder.utils;

public class FinderConstants {
    public static int DEFAULT_FILE_NUMBER = 3;
    public static int MAX_FILE_NUMBER = 40;

    public static String KEY_FILE_NUM = "file_num";
    public static String KEY_DIRECTORIES_LIST = "directories_list";
    public static String KEY_FILES_LIST = "files_list";
    public static String KEY_FILES_CHECKED = "files_checked";

    public static String KEY_RESULT_INTENT = "search_result";
    public static String KEY_PROGRESS_INTENT = "search_progress";
}
