package com.stepanov.esetfilefinder.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.documentfile.provider.DocumentFile;

import com.stepanov.esetfilefinder.R;
import com.stepanov.esetfilefinder.utils.FinderConstants;
import com.stepanov.esetfilefinder.utils.OptimizedFile;
import com.stepanov.esetfilefinder.utils.SortedFileList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BigFileFinderService extends Service {

    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private int filesNum;
    private SortedFileList sortedFileList;
    private int checkedFiles = 0;
    private int filesUntilSendBroadcast = 0;
    private Thread mainTread;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        filesNum = intent.getIntExtra(FinderConstants.KEY_FILE_NUM, 0);
        sortedFileList = new SortedFileList(filesNum);

        createNotificationChannel();

        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        mBuilder.setContentTitle("Searching for big files")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentText("Scanned 0 files")
                .setProgress(0, 1, true);
        mNotifyManager.notify(1, mBuilder.build());

        // Staring main function
        searchBigFiles(intent.getParcelableArrayListExtra(FinderConstants.KEY_DIRECTORIES_LIST));

        return super.onStartCommand(intent, flags, startId);
    }

    private void searchBigFiles(List<Uri> uris) {
        // Creating thread for searching
        mainTread = new Thread() {
            @Override
            public void run() {
                List<DocumentFile> initDirectories = new LinkedList<>();
                // Converting Uris to DocumentFiles
                for (Uri uri : uris) {
                    initDirectories.add(DocumentFile.fromTreeUri(getApplicationContext(), uri));
                }
                for (DocumentFile file : initDirectories) {
                    if (mainTread.isInterrupted()) {
                        return;
                    }
                    scanFile(file);
                }

                // Sending result intent after scanning
                Intent resultIntent = new Intent(FinderConstants.KEY_RESULT_INTENT);
                ArrayList<Uri> paths = new ArrayList<>();
                for (OptimizedFile f : sortedFileList) {
                    paths.add(f.uri);
                }

                resultIntent.putParcelableArrayListExtra(FinderConstants.KEY_FILES_LIST, paths);
                sendBroadcast(resultIntent);
                mNotifyManager.cancel(1); // deleting notification
                stopSelf();
            }
        };
        mainTread.start();
    }

    private void scanFile(DocumentFile file) {
        if (mainTread.isInterrupted()) {
            return;
        }
        if (file == null)
            return;
        if (file.isFile()) {
            // Check as file
            if (sortedFileList.size() < filesNum) {
                // First N items will be in list for future comparing
                sortedFileList.add(new OptimizedFile(file.getUri(), file.length()));
            } else {
                // If list is full, then compare file with minimum file size in list
                if (sortedFileList.getMinimalFile().size < file.length()) {
                    sortedFileList.add(new OptimizedFile(file.getUri(), file.length()));
                }
            }
        } else {
            // Run check for directory
            for (DocumentFile innerFile : file.listFiles()) {
                scanFile(innerFile);
            }
        }
        checkedFiles++;
        filesUntilSendBroadcast--;
        // Sending notification every 20 checked files
        if (filesUntilSendBroadcast < 0) {
            Intent progressIntent = new Intent(FinderConstants.KEY_PROGRESS_INTENT);
            progressIntent.putExtra(FinderConstants.KEY_FILES_CHECKED, checkedFiles);
            sendBroadcast(progressIntent);
            mBuilder.setContentText("Scanned " + checkedFiles + " files");
            mNotifyManager.notify(1, mBuilder.build());
            filesUntilSendBroadcast = 20;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainTread.interrupt();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            mNotifyManager = getSystemService(NotificationManager.class);
            mNotifyManager.createNotificationChannel(serviceChannel);
        }
    }

}