package com.stepanov.esetfilefinder.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.slider.RangeSlider;
import com.stepanov.esetfilefinder.R;
import com.stepanov.esetfilefinder.ui.search.SearchFragment;
import com.stepanov.esetfilefinder.utils.FinderConstants;

/*
Fragment with slider for number of files and directories list
 */
public class MainFragment extends Fragment {

    private static final int FOLDER_REQUEST_CODE = 999;
    private TextView fileCounterTextView;
    private RangeSlider fileNumSlider;
    private Button startSearchingButton;
    private RecyclerView directoryList;
    private FloatingActionButton addDirectoryButton;
    private DirectoryListAdapter directoryListAdapter;
    private int fileNum = FinderConstants.DEFAULT_FILE_NUMBER;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initListeners();
        initAdapters();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FOLDER_REQUEST_CODE:
                if (data == null || data.getData() == null) {
                    break;
                }
                directoryListAdapter.addDirectory(DocumentFile.fromTreeUri(requireContext(), data.getData()));
                break;
        }
    }

    private void initViews(View view) {
        fileCounterTextView = view.findViewById(R.id.tvFileCounter);
        fileCounterTextView.setText(
                getString(R.string.number_of_files_searching, FinderConstants.DEFAULT_FILE_NUMBER));
        fileNumSlider = view.findViewById(R.id.sFileNumSlider);
        fileNumSlider.setValues((float) FinderConstants.DEFAULT_FILE_NUMBER);
        fileNumSlider.setValueTo((float) FinderConstants.MAX_FILE_NUMBER);
        startSearchingButton = view.findViewById(R.id.btnStartSearching);
        directoryList = view.findViewById(R.id.rvSelectedDirectories);
        addDirectoryButton = view.findViewById(R.id.btnAddDirectoryButton);
    }

    private void initListeners() {
        fileNumSlider.addOnChangeListener((slider, value, fromUser) -> {
            fileNum = (int) value;
            fileCounterTextView.setText(getString(R.string.number_of_files_searching, (int) value));
        });
        addDirectoryButton.setOnClickListener(btn -> {
            Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(i, "Choose directory"), FOLDER_REQUEST_CODE);
        });
        startSearchingButton.setOnClickListener(btn -> {
            SearchFragment searchFragment = new SearchFragment();

            Bundle args = new Bundle();
            args.putInt(FinderConstants.KEY_FILE_NUM, fileNum);
            args.putParcelableArrayList(FinderConstants.KEY_DIRECTORIES_LIST, directoryListAdapter.getSelectedDirectories());

            searchFragment.setArguments(args);

            requireActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flFragmentContainer, searchFragment)
                    .commit();
        });
    }

    private void initAdapters() {
        directoryListAdapter = new DirectoryListAdapter();
        directoryList.setAdapter(directoryListAdapter);
    }

}