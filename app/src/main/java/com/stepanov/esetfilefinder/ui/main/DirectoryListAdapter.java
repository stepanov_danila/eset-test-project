package com.stepanov.esetfilefinder.ui.main;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.RecyclerView;

import com.stepanov.esetfilefinder.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class DirectoryListAdapter extends RecyclerView.Adapter<DirectoryListAdapter.ViewHolder> {

    private final List<DocumentFile> data = new LinkedList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_directory, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addDirectory(DocumentFile newDirectory) {
        for (DocumentFile file : data) {
            if (file.getUri().equals(newDirectory.getUri())) {
                return;
            }
        }
        data.add(newDirectory);
        notifyItemInserted(getItemCount() - 1);
    }

    void removeDirectory(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public ArrayList<Uri> getSelectedDirectories() {
        ArrayList<Uri> result = new ArrayList<>();
        for (DocumentFile f : data) {
            result.add(f.getUri());
        }
        return result;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView directoryNameTextView = itemView.findViewById(R.id.tvDirectoryName);
        private final TextView directoryPathTextView = itemView.findViewById(R.id.tvDirectoryPath);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.findViewById(R.id.btnRemoveDirectory).setOnClickListener(btn ->
                    removeDirectory(getAdapterPosition()));
        }

        public void bind(DocumentFile item) {
            directoryNameTextView.setText(item.getName());
            directoryPathTextView.setText(item.getUri().getPath());
        }
    }
}
