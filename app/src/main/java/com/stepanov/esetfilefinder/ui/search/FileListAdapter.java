package com.stepanov.esetfilefinder.ui.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.stepanov.esetfilefinder.R;

import java.text.DecimalFormat;

class FileListAdapter extends ListAdapter<DocumentFile, FileListAdapter.ViewHolder> {

    static DiffUtil.ItemCallback<DocumentFile> diffCallback = new DiffUtil.ItemCallback<DocumentFile>() {
        @Override
        public boolean areItemsTheSame(@NonNull DocumentFile oldItem, @NonNull DocumentFile newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getUri().equals(newItem.getUri());
        }

        @Override
        public boolean areContentsTheSame(@NonNull DocumentFile oldItem, @NonNull DocumentFile newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getUri().equals(newItem.getUri()) &&
                    oldItem.length() == newItem.length();
        }
    };

    protected FileListAdapter() {
        super(diffCallback);
    }

    @NonNull
    @Override
    public FileListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FileListAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FileListAdapter.ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView fileNameTextView = itemView.findViewById(R.id.tvFileItemName);
        private final TextView filePathTextView = itemView.findViewById(R.id.tvFileItemPath);
        private final TextView fileSizeTextView = itemView.findViewById(R.id.tvFileItemSize);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(DocumentFile item) {
            fileNameTextView.setText(item.getName());
            filePathTextView.setText(item.getUri().getPath());
            fileSizeTextView.setText(formatFileSize(item.length()));
        }

        private String formatFileSize(long fileSize) {
            if (fileSize <= 0) return "0";
            final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
            int digitGroups = (int) (Math.log10(fileSize) / Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(fileSize / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
        }
    }
}
