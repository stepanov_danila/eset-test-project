package com.stepanov.esetfilefinder.ui.search;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.stepanov.esetfilefinder.R;
import com.stepanov.esetfilefinder.service.BigFileFinderService;
import com.stepanov.esetfilefinder.ui.main.MainFragment;
import com.stepanov.esetfilefinder.utils.FinderConstants;

import java.util.LinkedList;
import java.util.List;

/*
Fragment with progress indication and result list
 */
public class SearchFragment extends Fragment {

    private TextView progressTextView;
    private final BroadcastReceiver progressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int filesChecked = intent.getIntExtra(FinderConstants.KEY_FILES_CHECKED, 0);
            progressTextView.setText(getString(R.string.scanned_x_files, filesChecked));
        }
    };
    private Button cancelButton;
    private Button startNewSearchButton;
    private RecyclerView resultFilesList;
    private LinearProgressIndicator progressIndicator;
    private FileListAdapter fileListAdapter;
    private final BroadcastReceiver resultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            List<Uri> paths = intent.getParcelableArrayListExtra(FinderConstants.KEY_FILES_LIST);
            List<DocumentFile> results = new LinkedList<>();
            for (Uri uri : paths) {
                results.add(0, DocumentFile.fromSingleUri(requireContext(), uri));
            }
            fileListAdapter.submitList(results);
            progressTextView.setVisibility(View.GONE);
            progressIndicator.setVisibility(View.GONE);
            cancelButton.setVisibility(View.GONE);
            startNewSearchButton.setVisibility(View.VISIBLE);
        }
    };


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initListeners();
        initAdapters();

        // Starting main service that will scan directories
        Intent newServiceIntent = new Intent(requireContext(), BigFileFinderService.class);
        newServiceIntent.putExtra(FinderConstants.KEY_FILE_NUM, getArguments().getInt(FinderConstants.KEY_FILE_NUM, 0));
        newServiceIntent.putStringArrayListExtra(
                FinderConstants.KEY_DIRECTORIES_LIST,
                getArguments().getStringArrayList(FinderConstants.KEY_DIRECTORIES_LIST));
        requireContext().startService(newServiceIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        requireActivity().registerReceiver(resultReceiver, new IntentFilter(FinderConstants.KEY_RESULT_INTENT));
        requireActivity().registerReceiver(progressReceiver, new IntentFilter(FinderConstants.KEY_PROGRESS_INTENT));
    }

    @Override
    public void onPause() {
        super.onPause();
        requireActivity().unregisterReceiver(resultReceiver);
        requireActivity().unregisterReceiver(progressReceiver);
    }

    private void initViews(View view) {
        progressTextView = view.findViewById(R.id.tvSearchProgressText);
        cancelButton = view.findViewById(R.id.btnCancelSearch);
        startNewSearchButton = view.findViewById(R.id.btnStartNewSearch);
        progressIndicator = view.findViewById(R.id.lpiSearchProgress);
        resultFilesList = view.findViewById(R.id.rvResultFilesList);
    }

    private void initListeners() {
        cancelButton.setOnClickListener(btn -> stopSearching());
        startNewSearchButton.setOnClickListener(btn -> {
            stopSearching();
            requireActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flFragmentContainer, new MainFragment())
                    .commit();
        });
    }

    private void initAdapters() {
        fileListAdapter = new FileListAdapter();
        resultFilesList.setAdapter(fileListAdapter);
    }

    private void stopSearching() {
        requireContext().stopService(new Intent(requireContext(), BigFileFinderService.class));
    }
}
