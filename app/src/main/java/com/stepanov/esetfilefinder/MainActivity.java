package com.stepanov.esetfilefinder;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.stepanov.esetfilefinder.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            // Starting with main fragment
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flFragmentContainer, new MainFragment())
                    .commitNow();
        }
    }
}