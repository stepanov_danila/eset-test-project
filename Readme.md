# ESET Test Project Solution by Danylo Stepanov



## Application Structure

Application contains single Main Activity with two fragments and one service.  
MainFragment responsible for selecting number of files to be selected and list of directories.  
SearchFragment responsible for displaying results of searching process.  
BigFileFinderService responsible for searching files and sending notification.  

## Logic
I tried to come up with an algorithm that would do this in the most efficient way. And I got the idea that we can store N large files in a sorted list. Then the smallest of these large files will be at position 0. When comparing files, we compare the current file with the file at position 0. And if it is greater than it, then we insert it at the right place.

I thought it would be too long to go through all the files and folders. Therefore, I came up with an idea - if the weight of the entire folder is less than the file at position zero (assuming the list of N elements is full), then it makes no sense for us to scan this folder. Since its total size is less than our minimum element in maximum files list. But I couldn't implement it because I couldn't calculate the folder size efficiently.

## Progress Indication
I also made a progress indication in the form of an inscription about the number of checked files and an endless progress bar. Notification in status bar also has indeterminate progress bar with info about checked files amount. It's very simple to calculate progress when we scan directory with files only. And I did it. But in case when we have subdirectories it's became harder to calculate total amount of files. So I decided to put endless status bar instead of progress bar with step.

## Background
I work mostly with Kotlin projects, so it was a bit difficult for me to write everything from scratch in Java. I abandoned DI because I can only make them in Kotlin, I also abandoned the MVVM architecture for the same reasons. I understand that maybe my job will be to support or create solutions in Java, so I am ready to continue learning and effectively apply knowledge in both languages. There was not enough time to write efficient Java code.
